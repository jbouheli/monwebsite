const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const formParser = require("body-parser");

// je créer les routes vers mes pages ici
const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");
const huguesRouter = require("./routes/hugues-capet");
const louisRouter = require("./routes/louis");
const augusteRouter = require("./routes/auguste");
const lebelRouter = require("./routes/lebel");
const formRouter = require("./routes/formulaire");

const app = express();

// j'ajoute le dossier "views" à toute mes pages html pour que ca marche
app.set("views", path.join(__dirname, "views"));
// je definis le template utilisé pour mes vues, ici "ejs"
app.set("view engine", "ejs");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(formParser.urlencoded({ extended: true }));

//j'envoi les variables
app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/hugues-capet", huguesRouter);
app.use("/louis", louisRouter);
app.use("/auguste", augusteRouter);
app.use("/lebel", lebelRouter);
app.use("/formulaire", formRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
