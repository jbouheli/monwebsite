let formu = [];
// example {id:1592304983049, title: 'Deadpool', year: 2015}
const dataFormu = ev => {
  ev.preventDefault(); //to stop the form submitting
  let formul = {
    id: Date.now(),
    title: document.getElementById("title").value,
    year: document.getElementById("yr").value
  };
  formu.push(formul);
  document.forms[0].reset(); //
  //document.querySelector('form').reset();
  //for display purposes only
  console.warn("added", { formu });
  let pre = document.querySelector("#msg pre");
  pre.textContent = "\n" + JSON.stringify(formu, "\t", 2);
  //saving to localStorage
  localStorage.setItem("utilisateur", JSON.stringify(formu));
};
document.addEventListener("DOMContentLoaded", () => {
  document.getElementById("btn").addEventListener("click", dataFormu);
});
