const express = require("express");
const router = express.Router();

/* GET home page. */
router.get("/", function(req, res, next) {
  res.render("formulaire", { page: "S'inscrire", menuId: "formulaire" });
});

module.exports = router;
