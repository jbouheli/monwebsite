const express = require("express");
const router = express.Router();

/* GET home page. */
router.get("/", function(req, res, next) {
  res.render("auguste", { page: "Phillipe Auguste", menuId: "auguste" });
});

module.exports = router;
