const express = require("express");
const router = express.Router();

/* GET home page. */
router.get("/", function(req, res, next) {
  res.render("lebel", { page: "Phillipe le Bel", menuId: "lebel" });
});

module.exports = router;
