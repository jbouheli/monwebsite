const express = require("express");
const router = express.Router();

/* GET home page. */
router.get("/", function(req, res, next) {
  res.render("louis", { page: "Louis IX", menuId: "louis" });
});

module.exports = router;
